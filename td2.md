## Lab 2: Unitary tests

### Preliminaries

- Create a gitlab account
- Create a public repository in your gitlab account named: m4207
- Please leave a message in the issue: [ISSUE NUMBER 1](https://gitlab.com/m4207/td/issues/1), if not done yet.
- Every bit of codes, comments, documentation should be stored on your project
- At the end of each session, commit  (`git add`, `git commit`), tag `git tag TD2`) and push your work (`git push --tags`, `git push`). 

__If you find any errors, have any tips, tricks, advices, please open an issue__

__Put a picture on your gitlab profile, A REAL PICTURE, so that I can identify you__


### Important side notes

Since you are going to work in parallel in your group you have to implement some mechanims
to optimize your work. The most important part here is to implement a way to communicate.

You have also to let us know the composition of your group. To do that, drop a message with yout team member list
on [ISSUE NUMBER 2](https://gitlab.com/m4207/td/issues/2). If you create a specific repository for your project, 
invite me to be part of it (my login is: trazafin on gitlab and trazafin on github if you choose github). 


### Where are so so far ? .

For now, you have a blinking program running on your board from the previous Lab. You
should now know out to update the firmware of the board, compile and upload your code.
(This is just a cliking and a configuration game.)


#### EX. 0: Serial Port
In this part of the lab we are going to test the serial port. You objective is to  write
(or copy/paste) a code that print "Hello World" to the serial port, with a blinking LED.

Be aware that sometimes you need to have a delay before starting the serial connection.


#### EX. 1: Basic skills...

In this part of the lab we are going to create a program to show you skills (may not be copy paste). 
You objective is to  write a code that create a counter and print the following line (loop) on
the serial output: `the value of the counter is x`, where `x` increments by `1` every seconds. 


Upload your code and put the results of the serial output in a readme file. 


#### EX. 2: GPS test

In this part of the lab we are going to test the GPS. You objective is to  write
(or copy/paste) a code that prints the output of the GPS on the serial link. 

Upload your code and put the results of the serial output in a readme file. 




#### EX. 3: WIFI test

In this part of the lab we are going to test the WiFi. You objective is to  write
(or copy/paste) a code that prints WiFi status on the serial link. These status
include the AP name to which the board is connected, the IP address, IP mask, IP
gateway, and the received signal strength value.

These may need some more configuration. For an easy test, use you phone as an Access
Point. 

Upload your code and put the results of the serial output in a readme file. 




#### EX. 4: Data Storage: Write process

In this part of the lab we are going to test the Data Storage. You objective is to  write
(or copy/paste) a code that prints GPS or WiFi status on the Internal 10M Flash. 

Upload your code and put the results of the serial output in a readme file. 




#### EX. 5: Data Storage: Read process

In this part of the lab we are going to test the Data Storage. You objective is to  write
(or copy/paste) a code that reads a file from the Internal 10M Flash and write it to the
serial output.

Upload your code and put the results of the serial output in a readme file. 




#### EX. 6: Web of Things

In this part of the lab we are going to create a WebServer through WiFi. You objective is to  write
(or copy/paste) a code that serve a static page with "Hello World" content. This web server 
should be accessible through a WiFi connection.

Upload your code and put the results of the serial output in a readme file. 



#### EX. 7: Bonus

Based on the previous WebServer, serve do data text stored on your Flash memory. You have first to
create the file that contains the content you wan to serve. It could be a simple "Hello World". 

Upload your code and put the results of the serial output in a readme file. 
