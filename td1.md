## Lab 1: This is a two steps lab. 

#### Introduction to computer architecture. 

___A micro-controller:___

A micro-controller is a true computer on a chip. The design incorporates all of 
the features found in a micro-processor CPU: ALU, PC, SP, and registers. It also 
has ROM, RAM, parallel I/O, serial I/O, counters and a clock circuit. The 
micro-controller is a general-purpose device meant to read data, perform limited 
calculations on that data and control its environment based on those calcualtions. 
The prime use of a microcontroller is to control the operations of a machine 
using a fixed program that is stored in ROM and does not change over the 
lifetime of the system. The microcontroller is concerned with getting data from 
and to its own pins; the architecture and instruction set are optimized to 
handle data in bit and byte size. 

___Build everything from scratch___
- slides about logic gate intro to flip-flop [here](https://gitlab.com/m4207/td/raw/master/files/m4207-td00.pdf)
- slides about flip-flop, counter, ram and first basic calculator [here](https://gitlab.com/m4207/td/raw/master/files/m4207_td02.pdf)
- slides about general purpose computer, intro to assembly language [here](https://gitlab.com/m4207/td/raw/master/files/m4207_td03.pdf)

#### Developpement environment. 

___Unfortunately, the IDE of the board is not fully functionnal under linux (virtual)___

___Before using the board for the first time, it is mandatory to flash it using MAC or Windows___

Please follw the installation of the IDE from this page: [mediatek doc](https://docs.labs.mediatek.com/resource/linkit-one/en/getting-started)

##### Below is a fast summary of the installation under MACOSX:

- Download [Java SE Runtime Environment 7](http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html) and install it.
- Download the latest Arduino IDE [here](https://www.arduino.cc/en/Main/Software) and install it. 
- Download the USB COM port driver for the LinkIt ONE development board [here](https://labs.mediatek.com/en/download/I2cGRHeR) and install it.
- Install LinkIt ONE SDK with Arduino Board Manager 
    1. In the Arduino IDE, on the File menu click Preferences.
    2. In Additional Boards Manager URLs enter: http://download.labs.mediatek.com/package_mtk_linkit_index.json
    3. Make sure your computer is connected to the internet.
    4. In the Arduino Tools menu point to Board then click Boards Manager. Arduino IDE now downloads the additional 
        Boards Manager details (the .json file), which may take a few seconds.
    5. LinkIt ONE by Seeed Studios and MediaTek Labs is now displayed in Boards Manager. 
        Select the latest LinkIt ONE SDK version and click Install. 
    6. When installation completes version n.n.nn INSTALLED is appended to the tool’s name, as shown below. 
- Update your board’s firmware on OS X
    1. In Arduino IDE, on the Tools menu point to Board and click LinkIt ONE, to configure the LinkIt ONE board as the target. 
    2. In Arduino IDE, on the Tools menu point to Programmer and click LinkIt Firmware Updater as shown below: 
    3. Set the board to Mass Storage Bootup mode
    4. Connect the LinkIt ONE development board to your computer using a micro-USB cable.
    5. In Arduino IDE, on the Tools menu click Burn Bootloader to launch the firmware updater application. 
    6. In LinkIt ONE Firmware Updater, click the download button
    7. Follow instruction (unplug, wait, switch the board to normal bootup)
- Configure the Arduino IDE on OS X
    1. Set the board to Normal Bootup Mode
    2. Connect the LinkIt ONE development board to your Mac using a micro-USB cable.
    3. Change port to the appropriate one: tools > port > usbmodem (LinkitOne)
- Create and run your first sketch on OS X
    1. blinking example [here](https://docs.labs.mediatek.com/resource/linkit-one/en/getting-started/get-started-on-os-x/create-and-run-your-first-sketch-on-os-x) or below.
    2. Confirm that your board is in SPI mode, by checking the SPI/SD Card switch shown below. The switch should be in the position closest the LED.
    3. Upload your code using the button.
    4. Watch the LED blink on the board

##### Windows installation [here](https://docs.labs.mediatek.com/resource/linkit-one/en/getting-started/get-started-on-windows/get-the-hardware-and-software)
Not tested.

##### Linux Installation
- Install the Arduino IDE [here](https://www.arduino.cc/en/Main/Software)
- Installing LinkIt one packages to your arduino 
- Install LinkIt ONE SDK with Arduino Board Manager 
    1. In the Arduino IDE, on the File menu click Preferences.
    2. In Additional Boards Manager URLs enter: https://raw.githubusercontent.com/v-i-s-h/LinkIt-One-Linux-Arduino-Support-/master/package_vish_linkItOne_linux_index.json
    3. Make sure your computer is connected to the internet.
    4. In the Arduino Tools menu point to Board then click Boards Manager. Arduino IDE now downloads the additional 
        Boards Manager details (the .json file), which may take a few seconds.
    5. LinkIt ONE by Seeed Studios and MediaTek Labs is now displayed in Boards Manager. 
        Select the latest LinkIt ONE SDK version and click Install. 
    6. When installation completes version n.n.nn INSTALLED is appended to the tool’s name, as shown below.
    7. ```/home/rt/.arduino15/packages"``` (this in my Ubuntu 14.04), you will be able to see a folder ```LinkItOneLinuxArduino```.
- Goto ```cd /home/rt/.arduino15/packages/LinkItOneLinuxArduino/tools/linkit_tools/1.1.17``` through command line 
    and execute ```chmod a+x *.sh```. This is required to make the downloaded tools executable.
- Download: [packtag.py](https://raw.githubusercontent.com/Seeed-Studio/Lua_for_RePhone/master/tools/packtag.py) or [here](https://gitlab.com/m4207/td/raw/master/files/packtag.py) and move it to 
    ```/home/rt/.arduino15/packages/LinkItOneLinuxArduino/tools/linkit_tools/1.1.17```
- Run ```chmod +x packtag.py```
- Download [platform.txt](https://gitlab.com/m4207/td/raw/master/files/platform.txt) and move it to: 
    ```/home/rt/.arduino15/packages/LinkItOneLinuxArduino/hardware/arm/1.1.17```
- Change board in tools > board select Linkit One
- Compile a new sketch, use the ```verify``` button
- By now you have successfully compiled your sketch. To upload, you don't have the option of direct USB upload. Instead follow this procedure.
    1. In Arduino IDE, goto Sketch > Export compiled Binary. This will again compile your sketch and output a file ```app.vxp``` to your sketch folder.
    2. Goto Sketch > Show Sketch folder to view your folder. There must be a file ```app.vxp``` in it.
    3. Now in your LinkIt One change ```UART/MS``` slider to mass storage ```MS``` position and connect to your PC. 
    4. It should show as 10 MB Volume (removable drive).
    5. Copy 'app.vxp' to 'MRE' folder in your mounted LinkIt One
    6. At the root of LinkIt One drive, edit ```autostart.txt```. Change the second line to ```App=C:\MRE\app.vxp```
    7. Unmout your Board and switch back to normal boot-up (UART)
- You're done.

#### Our micro-controller: Linkit One. 

The specification of our board is [here](https://gitlab.com/m4207/td/raw/master/files/LinkIt_ONE_User_Manual.pdf)

#### Code example
```c
void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  delay(100);
  digitalWrite(13,LOW);
  delay(100);
}
```



### Next steps !!!

- Create a gitlab account
- Create a public repository in your gitlab account named: m4207
- Please leave a message in the issue: [ISSUE NUMBER 1](https://gitlab.com/m4207/td/issues/1), if not done yet.
- Every bit of codes, comments, documentation should be stored on your project
- At the end of each session, commit  (`git add`, `git commit`), tag `git tag TD3`) and push your work (`git push --tags`, `git push`). 

__If you find any errors, have any tips, tricks, advices, please open an issue__

__Put a picture on your gitlab profile, A REAL PICTURE, so that I can identify you__


### Important side notes


Since you are going to work in parallel in your group you have to implement some mechanims
to optimize your work. The most important part here is to implement a way to communicate.

You have also to let us know the composition of your group. To do that, drop a message with yout team member list
on [ISSUE NUMBER 2](https://gitlab.com/m4207/td/issues/2). If you create a specific repository for your project, 
invite me to be part of it (my login is: trazafin on gitlab and trazafin on github if you choose github). 


